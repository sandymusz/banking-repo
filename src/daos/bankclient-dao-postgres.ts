import { Account, BankClient } from "../entities";
import { BankClientDAO } from "./bankclient-dao";
import { client } from "../connection";
import { MissingClientError } from "../errors";

export class BankClientDaoPostgres implements BankClientDAO{
    
    async createBankClient(bankClient: BankClient): Promise<BankClient> {
        const sql:string = "insert into bankclient(fname,lname) values ($1,$2) returning bankclient_id";
        const values = [bankClient.fname,bankClient.lname];
        const result = await client.query(sql,values);
        bankClient.bankClientId = result.rows[0].bankclient_id;
        
        //console.log("ASH: DEBUG create new bank client :"+bankClient.bankClientId);
        return bankClient;
    }
    async getAllBankClients(): Promise<BankClient[]> {
        const sql:string = 'select * from bankclient';
        const result = await client.query(sql);
        const bankClients:BankClient[] = []
        for(const row of result.rows){
            const bankClient:BankClient = new BankClient(
                row.bankclient_id,
                row.fname,
                row.lname);
            bankClients.push(bankClient);
        }
        return bankClients;
    }
    async getBankClientById(bankClientId: number): Promise<BankClient> {
        const sql:string = 'select * from bankclient where bankclient_id = $1';
        const values = [bankClientId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingClientError(`The client with ID ${bankClientId} does not exist`);
        }
        const row = result.rows[0];
        const bankClient:BankClient = new BankClient(
            row.bankclient_id,
            row.fname,
            row.lname);
        return bankClient;
    }
    async updateBankClient(bankClient: BankClient): Promise<BankClient> {
        const sql:string = 'update bankclient set fname=$1, lname=$2 where bankclient_id=$3';
        const values = [bankClient.fname,bankClient.lname,bankClient.bankClientId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingClientError(`The client with ID ${bankClient.bankClientId} doed not exist`);
        }
        return bankClient;
    }

    async deleteBankClientById(bankClientId: number): Promise<boolean> {
        const sql:string = 'delete from bankclient where bankclient_id=$1';
        const values = [bankClientId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingClientError(`The client with ID ${bankClientId} does not exist`);
        }
        return true
    }
    createAccount(account: Account): Promise<Account> {
        throw new Error("Method not implemented.");
    }
    updateAccountAmount(amount: Account): Promise<Account> {
        throw new Error("Method not implemented.");
    }
    deleteAccountById(accountId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    getAllAccounts(): Promise<Account[]> {
        throw new Error("Method not implemented.");
    }
}