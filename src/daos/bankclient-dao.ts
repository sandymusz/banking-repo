import { BankClient, Account } from "../entities";


export interface BankClientDAO{
   
    // CREATE
    createBankClient(bankClient:BankClient):Promise<BankClient>;
    createAccount(account:Account):Promise<Account>; //how to create a new account for a client?

    // READ
    getAllBankClients():Promise<BankClient[]>;
    getBankClientById(bankClientId:number): Promise<BankClient>;
    // getAccountByAmount
    // getAccountById
    getAllAccounts():Promise<Account[]>; //how to get accounts of a Client?
    //how to get all accounts with amount between 400 and 2000


    // UPDATE/PUT
    updateBankClient(bankClient:BankClient):Promise<BankClient>;
    updateAccountAmount(amount:Account):Promise<Account>;

    // DELETE
    deleteBankClientById(bankClientId:number):Promise<boolean>;
    deleteAccountById(accountId:number):Promise<boolean>;
 }