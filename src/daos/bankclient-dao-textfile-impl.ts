import { Account, BankClient } from "../entities";
import { BankClientDAO } from "./bankclient-dao";
import {readFile, writeFile} from "fs/promises";
import { MissingClientError } from "../errors";


export class BankClientDaoTextFile implements BankClientDAO{
    

    async createBankClient(bankClient: BankClient): Promise<BankClient> {
        const fileData: Buffer = await readFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt');
        const textData:string = fileData.toString(); // turn file into character data
        const bankClients:BankClient[] = JSON.parse(textData); // take the JSON text and turn it into an object
        bankClient.bankClientId = Math.round(Math.random()*1000); // random number id for the book
        bankClients.push(bankClient); // add our book to the array
        await writeFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt', JSON.stringify(bankClients));
        return bankClient;
    }
    async getAllBankClients(): Promise<BankClient[]> {
        const fileData: Buffer = await readFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt');
        const textData:string = fileData.toString(); 
        const bankClients:BankClient[] = JSON.parse(textData); 
        return bankClients;
    }
    async getBankClientById(bankClientId: number): Promise<BankClient> {
        const fileData: Buffer = await readFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt');
        const textData:string = fileData.toString(); 
        const bankClients:BankClient[] = JSON.parse(textData); 

        for(const bankClient of bankClients){
            if(bankClient.bankClientId === bankClientId){
                return bankClient;
            }
        }
        throw new MissingClientError(`The Client ID ${bankClientId} could not be located`);
    }
    async updateBankClient(bankClient: BankClient): Promise<BankClient> {
        const fileData: Buffer = await readFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt');
        const textData:string = fileData.toString(); 
        const bankClients:BankClient[] = JSON.parse(textData); 
        
        for(let i=0; i< bankClients.length; i++){
            if(bankClients[i].bankClientId === bankClient.bankClientId){
                bankClients[i] = bankClient;
            }
        }
        await writeFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt', JSON.stringify(bankClients));
        return bankClient;
        throw new MissingClientError(`The Client ID ${bankClient} could not be located`);
    }

    async deleteBankClientById(bankClientId: number): Promise<boolean> {
        const fileData: Buffer = await readFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt');
        const textData:string = fileData.toString(); 
        const bankClients:BankClient[] = JSON.parse(textData); 
        
        for(let i=0; i< bankClients.length; i++){
            if(bankClients[i].bankClientId === bankClientId){
                bankClients.splice(i)
                await writeFile('//Users//sandra.muszynski//Desktop//Project_0//src//bank-clients.txt', JSON.stringify(bankClients));
                return true;
            }
        }
        throw new MissingClientError(`The Client ID ${bankClientId} could not be located`);
        return false;
    }
    
    createAccount(account: Account): Promise<Account> {
        throw new Error("Method not implemented.");
    }

    getAllAccounts(): Promise<Account[]> {
        throw new Error("Method not implemented.");
    }

    updateAccountAmount(amount: Account): Promise<Account> {
        throw new Error("Method not implemented.");
    }
    
    deleteAccountById(accountId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
   
}