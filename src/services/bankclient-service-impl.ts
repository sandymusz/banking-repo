import { BankClientDAO } from "../daos/bankclient-dao";
import { BankClientDaoPostgres } from "../daos/bankclient-dao-postgres";
import { BankClientDaoTextFile } from "../daos/bankclient-dao-textfile-impl";
import { Account, BankClient } from "../entities";
import BankClientService from "./bankclient-service";


export class BankClientServiceImpl implements BankClientService{

    bankClientDAO:BankClientDAO = new BankClientDaoPostgres()
    //bankClientDAO:BankClientDAO = new BankClientDaoTextFile()
    
    createBankClient(bankClient: BankClient): Promise<BankClient> {
        return this.bankClientDAO.createBankClient(bankClient);
    }

    retrieveAllBankClients(): Promise<BankClient[]> {
        return this.bankClientDAO.getAllBankClients();
    }

    retrieveBankClientById(bankClientId: number): Promise<BankClient> {
        return this.bankClientDAO.getBankClientById(bankClientId);
    }

    updateBankClient(bankClient: BankClient): Promise<BankClient> {
        return this.bankClientDAO.updateBankClient(bankClient);
    }

    removeBankClientById(bankClientId: number): Promise<boolean> {
        return this.bankClientDAO.deleteBankClientById(bankClientId);
    }

    createAccount(account: Account): Promise<Account> {
        return this.bankClientDAO.createAccount(account);
    }

    removeAccountById(accountId: number): Promise<boolean> {
        return this.bankClientDAO.deleteAccountById(accountId);
    }
    

}