import { Account, BankClient } from "../entities";

export default interface BankClientService{

        createBankClient(bankClient:BankClient):Promise<BankClient>;

        retrieveAllBankClients():Promise<BankClient[]>;
        
        retrieveBankClientById(bankClientId:number):Promise<BankClient>;

        updateBankClient(bankClient:BankClient):Promise<BankClient>;

        removeBankClientById(bankClientId:number):Promise<boolean>;

        createAccount(account:Account):Promise<Account>;

        removeAccountById(accountId:number):Promise<boolean>;

}