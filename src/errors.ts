

export class MissingClientError {

    message:string;
    description:string = "ERROR Client could not be found";

    constructor(message:string){
        this.message = message;
    }
}