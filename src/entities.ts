

 export class BankClient {
     constructor(
         public bankClientId:number,
         public fname:string,
         public lname:string
         ){}

}

export class Account {
    constructor(
        public accountId:number,
        public accountType:string, 
        public accountAmount:number,
        public accountIsOpen:boolean,
        public bankClientId:number
        ){}

}
