import express from "express";
import { BankClient } from "./entities";
import { MissingClientError } from "./errors";
import BankClientService from "./services/bankclient-service";
import { BankClientServiceImpl } from "./services/bankclient-service-impl";


const app = express();
app.use(express.json());

const bankClientService:BankClientService = new BankClientServiceImpl();

app.post("/bankclients", async (req, res)=>{
    let bankClient:BankClient = req.body;
    console.log(bankClient.bankClientId);
    bankClient = await bankClientService.createBankClient(bankClient);
    res.send(bankClient);
});

app.get("/bankclients", async (req, res) => {
    try {
        const bankClients:BankClient[] = await bankClientService.retrieveAllBankClients();
        res.send(bankClients);
    } catch(error){

        if(error instanceof MissingClientError){
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/bankclients/:id", async (req, res)=>{
    try {
    const bankClientId = Number(req.params.id);
    const bankClient:BankClient = await bankClientService.retrieveBankClientById(bankClientId);
    res.send(bankClient);
    }
   catch (error) {
        if(error instanceof MissingClientError){
           res.status(404);
           res.send(error);
        }}
});

app.put("/bankclients/:id", async (req, res) =>{
    try {
        const bankClient = req.body;
        const bankClientId:number = Number(req.params.id);

        const retrievedClient:BankClient = await bankClientService.retrieveBankClientById(bankClientId);
        retrievedClient.bankClientId = bankClientId;
        retrievedClient.fname = bankClient.fname;
        retrievedClient.lname = bankClient.lname;
        const result:BankClient = await bankClientService.updateBankClient(retrievedClient);
        res.send(result);
    }
    catch (error) {
        if(error instanceof MissingClientError){
           res.status(404);
           res.send(error);
    }}
});

// app.delete("/bankclients/:id", async (req, res) =>{
//     try{const bankClient = req.body;
//         const bankClientId:number = Number(req.params.id);

//         const retrievedClient:BankClient = await bankClientService.retrieveBankClientById(bankClientId);
//         retrievedClient.bankClientId = bankClientId;
//         retrievedClient.fname = bankClient.fname;
//         retrievedClient.lname = bankClient.lname;
//     }
//     catch{
//     if(req.query.bankClientId) {
//       console.log(req.query.bankClientId);
//       await bankClientService.removeBankClientById(req.bankClientId);
//       res.status(200).send({Boolean});
//     } else {
//       res.status(400).send("Please specify a valid Client Id");
//     }}
//   });


app.listen(3000,()=>{console.log("Application Started")})