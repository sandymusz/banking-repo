import { isConditionalExpression } from "typescript";
import {client} from "../src/connection";
import { BankClientDAO } from "../src/daos/bankclient-dao";
import { BankClientDaoPostgres } from "../src/daos/bankclient-dao-postgres";
import { BankClientDaoTextFile } from "../src/daos/bankclient-dao-textfile-impl";
import { BankClient, Account } from "../src/entities";


const bankClientDAO:BankClientDAO = new BankClientDaoPostgres();
//const bankClientDao:BankClientDAO = new BankClientDaoTextFile();


const testBankClient:BankClient = new BankClient(0,'Albert','Einstien');
//const testAccount:Account = new Account(0,'Checking',5000,true,0);

test("Create a bank client", async ()=>{
    const result:BankClient = await bankClientDAO.createBankClient(testBankClient);
    console.log("Create Debug" +result.bankClientId);
    expect(result.bankClientId).not.toBe(0);
});

test("Get bank client by Id", async ()=>{
    let bankClient:BankClient = new BankClient(0,'Mike','Fish');
    bankClient = await bankClientDAO.createBankClient(bankClient);
    console.log("debugging: " +bankClient.bankClientId);
    console.log(bankClient.fname);
     let retrievedBankClient:BankClient = await bankClientDAO.getBankClientById(bankClient.bankClientId);

     expect(retrievedBankClient.fname).toBe(bankClient.fname);
});

test("Get all clients", async ()=>{
    let bankClient1:BankClient = new BankClient(0,'Charlie','Fischer');
    let bankClient2:BankClient = new BankClient(0,'Ashley','Goldstien');
    let bankClient3:BankClient = new BankClient(0,'Donald','Duck');
    let bankClient4:BankClient = new BankClient(0,'Rita','Bonita');
    await bankClientDAO.createBankClient(bankClient1);
    await bankClientDAO.createBankClient(bankClient2);
    await bankClientDAO.createBankClient(bankClient3);
    await bankClientDAO.createBankClient(bankClient4);

    const bankClients:BankClient[] = await bankClientDAO.getAllBankClients();

    expect(bankClients.length).toBeGreaterThanOrEqual(4);
});

test("update bank client", async ()=>{
    let bankClient:BankClient = new BankClient(0,'Amelia','Fish');
    bankClient = await bankClientDAO.createBankClient(bankClient);

    bankClient.lname = 'Williams';
    bankClient = await bankClientDAO.updateBankClient(bankClient)   
    
    expect(bankClient.lname).toBe('Williams');
});

test("delete bank client by id", async ()=>{
    let bankClient:BankClient = new BankClient(0,'Crisha','DeLeon');
    bankClient = await bankClientDAO.createBankClient(bankClient);

    const result:boolean = await bankClientDAO.deleteBankClientById(bankClient.bankClientId);
    expect(result).toBeTruthy()

});

afterAll(async ()=>{
    await client.end();
});

// test("update account", async ()=>{
//     let account:Account = new Account(0,0,1,2000,1);
//     account = await bankclientDAO.createAccount(account);

// })











